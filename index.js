var numberArr = [];
function themArr() {
  var number = document.querySelector("#txt-number").value * 1;
  numberArr.push(number);
  //document.querySelector("#txt-number").value = "";

  //Hiển thị mảng
  document.querySelector("#arrayHienTai").innerHTML = `<h5> ${numberArr} </h5>`;
}
// Câu 1
function tongSoDuong() {
  var tongSoDuong = 0;

  for (var index = 0; index < numberArr.length; index++) {
    var currentNumber = numberArr[index];
    if (currentNumber > 0) {
      tongSoDuong = tongSoDuong + currentNumber;
    }
  }

  document.querySelector(
    "#result1"
  ).innerHTML = ` <p>Tổng số dương: ${tongSoDuong}</p> `;
}

// Câu 2
function demSoDuong() {
  var soLuongsoDuong = 0;
  for (var index = 0; index < numberArr.length; index++) {
    var currentNumber = numberArr[index];
    if (currentNumber > 0) {
      soLuongsoDuong++;
    }
  }
  document.querySelector(
    "#result2"
  ).innerHTML = ` <p>Số lượng số dương: ${soLuongsoDuong}</p> `;
}

//Câu 3
function soMin() {
  // Gán số nhỏ nhất là số đầu tiên trong Mảng
  var soMin = numberArr[0];
  for (var index = 1; index < numberArr.length; index++) {
    // So sánh số nhỏ nhất đó và số tiếp theo trog mảng
    if (soMin > numberArr[index]) {
      soMin = numberArr[index];
    }
  }
  document.querySelector(
    "#result3"
  ).innerHTML = ` <p>Số nhỏ nhất: ${soMin}</p> `;
}

// Câu 4
function soMinDuong() {
  var soDuongArr = [];
  for (var index = 0; index < numberArr.length; index++) {
    var currentNumber = numberArr[index];
    if (currentNumber > 0) {
      soDuongArr.push(currentNumber);
    }
  }
  var soMinDuong = soDuongArr[0];
  for (var j = 1; j < soDuongArr.length; j++) {
    var currentIndex = soDuongArr[j];
    if (currentIndex < soMinDuong) {
      soMinDuong = currentIndex;
    }
  }
  document.querySelector(
    "#result4"
  ).innerHTML = `Số dương nhỏ nhất là: ${soMinDuong}`;
}

// Câu 5
function soChanCuoiCung() {
  for (var i = 0; i < numberArr.length; i++) {
    var soDuongHienTai = numberArr[i];
    if (soDuongHienTai % 2 == 0) {
      var soChanCuoiCung = soDuongHienTai;
      document.querySelector(
        "#result5"
      ).innerHTML = `Số chẵn cuối cùng là: ${soChanCuoiCung}`;
    } else {
      document.querySelector("#result5").innerHTML = `Số chẵn cuối cùng là: -1`;
    }
    for (var j = 1; j < numberArr.length; j++) {
      if (numberArr[j] % 2 == 0) {
        document.querySelector(
          "#result5"
        ).innerHTML = `Số chẵn cuối cùng là: ${numberArr[j]}`;
      }
    }
  }
}

// Câu 6
function doiCho() {
  var viTriSo1 = document.getElementById("viTriT1").value * 1;
  var viTriSo2 = document.getElementById("viTriT2").value * 1;

  // Tạo biến để lưu trữ khi đổi chỗ
  var temp;
  for (var index = 0; index < numberArr.length; index++) {
    temp = numberArr[viTriSo1];
    numberArr[viTriSo1] = numberArr[viTriSo2];
    numberArr[viTriSo2] = temp;
    document.querySelector(
      "#result6"
    ).innerHTML = `Mảng sau khi đổi chỗ: ${numberArr}`;
  }
}

// Câu 7
// tạo ra function so sánh 2 số
function soSanhHaiSo(a, b) {
  return a - b;
}
function sapXepTangDan() {
  // sử dụng hàm sort
  var mangSauKhiSapXep = numberArr.sort(soSanhHaiSo);
  document.querySelector(
    "#result7"
  ).innerHTML = `Mảng sau khi sắp xếp: ${mangSauKhiSapXep}`;
}

// Câu 8
function isSoNguyenTo(number) {
  // Số < 2 không phải số Nguyên Tố
  if (number < 2) {
    return false;
  }
  for (let i = 2; i < number; i++) {
    if (number % i == 0) {
      return false;
    }
  }
  return true;
}
function timSoNguyenTo() {
  for (let index = 0; index < numberArr.length; index++) {
    if (isSoNguyenTo(numberArr[index])) {
      document.getElementById(
        `result8`
      ).innerHTML = `Số nguyên tố đầu tiên: ${numberArr[index]}`;
      break;
    }
    document.getElementById(`result8`).innerHTML = `Số nguyên tố đầu tiên: -1`;
  }
}

// Câu 9
//Tạo 1 mảng mới
let newListCau9 = [];
var count = 0;

// thêm function cho nút Thêm Số
function addList() {
  var numberListCau9 = document.querySelector("#soNguyenList").value * 1;
  newListCau9.push(numberListCau9);

  //Hiển thị mảng câu 9
  document.querySelector("#arrayCau9").innerHTML = `<h5> ${newListCau9} </h5>`;
}
function demSoNguyen() {
  // Đặt biến đếm lại = 0 khi người dùng nhấn nút đếm nhưng chưa thêm số mới vào mảng
  count = 0;
  for (var index = 0; index < newListCau9.length; index++) {
    if (Number.isInteger(newListCau9[index])) {
      count++;
    }
  }
  document.getElementById(
    "result9"
  ).innerHTML = `<h5> Số nguyên: ${count} </h5>`;
}

// Câu 10
function soSanhAmDuong() {
  var soAm = 0,
    soDuong = 0;
  for (var index = 0; index < numberArr.length; index++) {
    if (numberArr[index] < 0) {
      soAm++;
    }
    if (numberArr[index] > 0) {
      soDuong++;
    }
    if (soAm > soDuong) {
      document.getElementById("result10").innerHTML = "Số âm > Số dương";
    } else if (soAm < soDuong) {
      document.getElementById("result10").innerHTML = "Số dương > Số âm";
    } else {
      document.getElementById("result10").innerHTML = "Số âm = Số số dương";
    }
  }
}
